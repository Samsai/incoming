extends KinematicBody2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

signal hit(damage)

onready var player_vars = get_node("/root/PlayerVariables")

# Called when the node enters the scene tree for the first time.
func _ready():
	add_to_group("player")
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.is_action_pressed("weapon_minigun"):
		player_vars.selected_weapon = player_vars.MINIGUN
	elif Input.is_action_pressed("weapon_flakcannon"):
		player_vars.selected_weapon = player_vars.FLAKCANNON
	
	pass


func _on_PlayerShip_hit(damage):
	print("hit, damage: " + str(damage))
	player_vars.health -= damage
	
	$HealthBar.value = player_vars.health
