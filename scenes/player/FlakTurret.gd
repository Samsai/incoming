extends Node2D

onready var player_vars = get_node("/root/PlayerVariables")

var gun: Node2D
var bullet_spawn: Position2D

var bullet = preload("res://scenes/player/FlakShell.tscn")
export var fire_rate: float = 2.0

var cooldown = 0.0

# Called when the node enters the scene tree for the first time.
func _ready():
	gun = get_node("Gun")
	bullet_spawn = get_node("Gun/Bullet_Spawn")
	
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):		
	var mouse_pos: Vector2 = get_viewport().get_mouse_position()
	var gun_pos: Vector2 = self.get_global_transform().origin
	
	self.gun.look_at(mouse_pos)
	
	if Input.is_action_pressed("fire_secondary") and self.cooldown <= 0.0 and player_vars.health > 0 and player_vars.flak_ammo > 0:
		$SFX.play()
		
		var new_bullet = bullet.instance()
		
		new_bullet.target = mouse_pos
		
		get_parent().add_child(new_bullet)
		
		new_bullet.global_transform = self.bullet_spawn.global_transform
		new_bullet.rotate(PI / 2)
		
		new_bullet.scale = Vector2(1.5, 1.5)
		
		new_bullet.set_fuze()
		
		self.cooldown = 1 / fire_rate
		player_vars.flak_ammo -= 1
	else:
		self.cooldown -= delta
		
	pass
