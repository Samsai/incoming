extends KinematicBody2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var start: Vector2
var target: Vector2
var velocity: Vector2
export var speed = 500

# Called when the node enters the scene tree for the first time.
#func _ready():
#	pass

func set_fuze():
	var distance: float = self.global_position.distance_to(target)
	var time_to_target: float = distance / speed
	$Fuze.wait_time = time_to_target
	$DeathTimer.wait_time = time_to_target + 1.0
	$Fuze.start()
	$DeathTimer.start()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var delta_x = sin(self.rotation) * speed
	var delta_y = -1 * cos(self.rotation) * speed 
	
	velocity = Vector2(delta_x, delta_y)
	var collision = self.move_and_collide(velocity * delta)

func _on_Fuze_timeout():
	$Explosion.explode()
	$Sprite.hide()
	speed = 0
	$ExplosionArea/CollisionShape2D.disabled = false

func _on_DeathTimer_timeout():
	queue_free()

func _on_ExplosionArea_body_entered(body):
	print("boom")
	body.emit_signal("hit", 30)
