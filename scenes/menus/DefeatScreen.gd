extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

onready var level_manager = get_node("/root/LevelManager")

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func enable():
	$Panel/VictoryButton.disabled = false
	
	self.show()
