extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

onready var level_manager = get_node("/root/LevelManager")

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func enable():
	$Panel/VictoryButton.disabled = false
	
	var current_level = get_tree().current_scene.level_index
	
	if level_manager.last_completed < current_level:
		level_manager.last_completed = current_level
		level_manager.save_game()
	
	self.show()
