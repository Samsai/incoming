extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

onready var player_vars = get_node("/root/PlayerVariables")

# Called when the node enters the scene tree for the first time.
func _ready():
	get_tree().paused = true
	$Popup.show()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.is_action_just_pressed("pause_menu"):
		get_tree().paused = false
		self.queue_free()
	pass


func _on_ResumeButton_pressed():
	get_tree().paused = false
	self.queue_free()


func _on_ReturnButton_pressed():
	player_vars.reset()
	get_tree().paused = false
	get_tree().change_scene("res://scenes/menus/LevelSelection.tscn")
