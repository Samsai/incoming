extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

onready var player_vars = get_node("/root/PlayerVariables")
onready var enemy_counter = get_node("/root/EnemyCounter")

# Called when the node enters the scene tree for the first time.
func _ready():
	player_vars.reset()
	enemy_counter.reset()
