extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export var level_name = "Level"
export var level_scene = ""
export var level_index = -1

onready var level_manager = get_node("/root/LevelManager")

# Called when the node enters the scene tree for the first time.
func _ready():
	$HBoxContainer/PlayButton.text = level_name
	
	if level_manager.last_completed + 1 >= level_index:
		$HBoxContainer/PlayButton.disabled = false
	if level_manager.last_completed >= level_index:
		$HBoxContainer/CheckBox.pressed = true
	pass


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_PlayButton_pressed():
	get_tree().change_scene(level_scene)
