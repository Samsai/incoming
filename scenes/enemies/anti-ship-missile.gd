extends KinematicBody2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Does the enemy counter need to keep track of this?
var counted = false
var flipped = false

onready var enemy_counter = get_node("/root/EnemyCounter")

export var health = 5
export var speed = 300

signal hit(damage)

# Called when the node enters the scene tree for the first time.
func _ready():
	add_to_group("enemies")


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var delta_x = sin(self.rotation)
	var delta_y = -1 * cos(self.rotation) 
	
	var current_direction = Vector2(delta_x, delta_y)
	
	var player = get_tree().get_nodes_in_group("player")[0]
	
	var player_pos: Vector2 = player.global_position
	var pos: Vector2 = self.global_position
	
	var direction_to_player = player_pos - pos
	
	var velocity = current_direction.slerp(direction_to_player, delta)
	
	self.rotate(current_direction.angle_to(velocity))
	
	var collision = self.move_and_collide(velocity * speed * delta)
	
	if collision and self.health > 0:
		collision.collider.emit_signal("hit", 20)
		emit_signal("hit", 100)
	
func _on_DeathTimer_timeout():
	if counted:
		enemy_counter.decrement_counter()
		
	self.queue_free()

func _on_AntiShipMissile_hit(damage):
	self.health -= damage
	
	if self.health <= 0:
		$Explosion.explode()
		$DeathTimer.start()
		
		self.speed = 0
		$Sprite.visible = false
		self.collision_mask = 0
		self.collision_layer = 0
