extends RigidBody2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var has_exploded = false
var counted = false
var flipped = false

# Called when the node enters the scene tree for the first time.
func _ready():
	if flipped:
		$Sprite.flip_v = true

func _on_Bomb_body_entered(body):
	if not has_exploded:
		has_exploded = true
		$Explosion.explode()
		$CollisionShape2D.disabled = true
		$Sprite.hide()
	
		body.emit_signal("hit", 20)


func _on_CleanUpTimer_timeout():
	queue_free()
