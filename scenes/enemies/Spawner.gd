extends Node2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export var activation_delay = 0.0
export var delay = 5.0
export var enemies = 3
export var flipped = false

export var enemy = preload("res://scenes/enemies/Jet.tscn")

onready var enemy_counter = get_node("/root/EnemyCounter")

# Called when the node enters the scene tree for the first time.
func _ready():
	$SpawnTimer.wait_time = delay
	$ActivationTimer.wait_time = activation_delay
	$ActivationTimer.start()
	
	enemy_counter.increment_counter(enemies)

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_SpawnTimer_timeout():
	if enemies > 0:
		print("Should have spawned something...")
		$SpawnLocation.offset = randi()
		
		var new_enemy = enemy.instance()
		new_enemy.flipped = flipped
		new_enemy.counted = true
		
		add_child(new_enemy)
		
		new_enemy.position = $SpawnLocation.position
		
		enemies -= 1


func _on_ActivationTimer_timeout():
	$SpawnTimer.start()
