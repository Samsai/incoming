extends KinematicBody2D

onready var enemy_counter = get_node("/root/EnemyCounter")

signal hit(damage)

export var health = 30
export var speed = 250
var falling = false
var counted = true
var flipped = false

var bomb = preload("res://scenes/enemies/bomb.tscn")

var reset_time = 2300 / speed

var horizontal_direction = -1

# Called when the node enters the scene tree for the first time.
func _ready():
	$ResetTimer.wait_time = reset_time
	$ResetTimer.start()
	
	horizontal_direction = 1 if flipped else -1
	
	if flipped:
		$Sprite.flip_h = true


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if falling:
		var collision = self.move_and_collide(Vector2(horizontal_direction, 0.5) * speed * delta)
		var direction = Vector2(cos(rotation), sin(rotation)).slerp(Vector2(cos(deg2rad(15 * horizontal_direction)), sin(deg2rad(15 * horizontal_direction))), delta*10)
		
		self.rotation = direction.angle()
		
		if collision:
			enemy_counter.decrement_counter()
			queue_free()
	else:	
		var collision = self.move_and_collide(Vector2(horizontal_direction, 0) * speed * delta)


func _on_PayloadTimer_timeout():
	var new_bomb = bomb.instance()
	
	if flipped:
		new_bomb.flipped = true
	
	get_parent().add_child(new_bomb)
	
	new_bomb.global_transform = $BombSpawn.get_global_transform()
	new_bomb.linear_velocity = Vector2(horizontal_direction, 0) * speed

func _on_ResetTimer_timeout():
	self.global_position += Vector2(-1 * horizontal_direction * speed * reset_time, 0)

func _on_Bomber_hit(damage):
	self.health -= damage
	
	if self.health <= 0:
		falling = true
		$Fire.emitting = true
		$ResetTimer.stop()
		$PayloadTimer.stop()
		self.collision_mask = 0
		self.set_collision_mask_bit(4, true)
