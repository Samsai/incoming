extends KinematicBody2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var velocity: Vector2
export var speed = 500
export var lifetime = 1.5

# Called when the node enters the scene tree for the first time.
func _ready():
	
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var delta_x = sin(self.rotation) * speed
	var delta_y = -1 * cos(self.rotation) * speed 
	
	velocity = Vector2(delta_x, delta_y)
	var collision = self.move_and_collide(velocity * delta)
	
	if collision:
		collision.collider.emit_signal("hit", 1)
		
		self.queue_free()
	
	lifetime -= delta
	
	if lifetime <= 0.0:
		self.queue_free()
