extends KinematicBody2D

onready var enemy_counter = get_node("/root/EnemyCounter")

signal hit(damage)

export var health = 30
export var speed = 150
export var flipped = false

var reset_time = 2300 / speed

export var rockets = 10
export var rocket_delay = 0.05
var rocket_cooldown = 0.0

var falling = false
var counted = true

var rocket = preload("res://scenes/enemies/dummy-rocket.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	$ResetTimer.wait_time = reset_time
	$ResetTimer.start()
	
	$Sprite.flip_h = flipped
	
	if flipped:
		rotation_degrees *= -1.0
		$Aim.cast_to *= -1.0

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var horizontal_direction = 1 if flipped else -1
	
	if falling:
		var collision = self.move_and_collide(Vector2(horizontal_direction, 0.5) * speed * delta)
		
		if collision:
			enemy_counter.decrement_counter()
			queue_free()
	else:
		var collision = self.move_and_collide(Vector2(horizontal_direction, 0) * speed * delta)
	
func _physics_process(delta):
	if $Aim.is_colliding() and rockets > 0 and rocket_cooldown <= 0:
		var player = $Aim.get_collider()
		
		var new_rocket = rocket.instance()
		get_parent().add_child(new_rocket)
		
		new_rocket.global_transform = self.global_transform
		
		if flipped:
			new_rocket.rotation_degrees = 90 + 25
		else:
			new_rocket.rotation_degrees = -90 - 25
		
		new_rocket.rotation_degrees += rand_range(-5, 5)
		
		rockets -= 1
		rocket_cooldown = rocket_delay
	else:
		rocket_cooldown -= delta


func _on_ResetTimer_timeout():
	var horizontal_direction = -1 if flipped else 1
	self.global_position += Vector2(horizontal_direction * speed * reset_time, 0)
	rockets = 10


func _on_Helicopter_hit(damage):
	health -= damage
	
	if health <= 0:
		falling = true
		$Fire.emitting = true
		$ResetTimer.stop()
		self.collision_mask = 0
		self.collision_layer = 0
		self.set_collision_mask_bit(4, true)
