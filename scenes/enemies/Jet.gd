extends KinematicBody2D

onready var enemy_counter = get_node("/root/EnemyCounter")

signal hit(damage)

export var health = 30
export var speed = 250

export var flipped = false
var horizontal_direction = -1

var payload_dropped = false
var falling = false
var counted = true

var reset_time = 2300 / speed

var missile = preload("res://scenes/enemies/anti-ship-missile.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	get_tree().get_root().add_to_group("enemies")
	
	$PayloadTimer.wait_time = rand_range(0, 3)
	$ResetTimer.wait_time = reset_time
	
	$PayloadTimer.start()
	$ResetTimer.start()
	
	$Sprite.flip_h = flipped
	# We need to flip the missile vertically, because the sprite is vertical
	$Sprite/Payload.flip_v = flipped
	
	horizontal_direction = 1 if flipped else -1
	
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if falling:
		var collision = self.move_and_collide(Vector2(horizontal_direction, 0.5) * speed * delta)
		var direction = Vector2(cos(rotation), sin(rotation)).slerp(Vector2(cos(deg2rad(15 * horizontal_direction)), sin(deg2rad(15 * horizontal_direction))), delta*10)
		
		self.rotation = direction.angle()
		
		if collision:
			enemy_counter.decrement_counter()
			queue_free()
	else:	
		var collision = self.move_and_collide(Vector2(horizontal_direction, 0) * speed * delta)


func _on_PayloadTimer_timeout():
	$Sprite/Payload.hide()
	
	var new_missile = missile.instance()
	
	get_parent().add_child(new_missile)
	
	new_missile.global_transform = $MissileSpawn.get_global_transform()
	
	if flipped:
		new_missile.rotate(PI)
	
	pass


func _on_ResetTimer_timeout():
	self.global_position += Vector2(-1 * horizontal_direction * speed * reset_time, 0)
	$PayloadTimer.wait_time = rand_range(0, 3)
	$PayloadTimer.start()
	$Sprite/Payload.show()
	
	pass


func _on_Jet_hit(damage):
	health -= damage
	
	if health <= 0:
		falling = true
		$Fire.emitting = true
		$ResetTimer.stop()
		self.collision_mask = 0
		self.set_collision_mask_bit(4, true)
