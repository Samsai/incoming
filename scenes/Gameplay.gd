extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
onready var enemy_counter = get_node("/root/EnemyCounter")
onready var player_vars = get_node("/root/PlayerVariables")

export var level_index = -1

var pause_menu = preload("res://scenes/menus/PauseMenu.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	player_vars.reset()
	pass


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if enemy_counter.total_enemies <= 0:
		$VictoryScreen.enable()
	elif player_vars.health <= 0:
		$DefeatScreen.enable()
	
	if Input.is_action_just_pressed("pause_menu"):
		add_child(pause_menu.instance())
