#+TITLE: Todo

* Cool game features
** Effects
*** DONE Clouds (possibly with particles)
** Enemies
*** Anti-ship missile
**** DONE Destructible
*** Fast-attack jet
**** DONE Destructible
*** Helicopter
**** DONE Destructible
**** TODO Better aiming?
*** Bomber planes
** Player weapons
*** DONE Switching between weapons
*** Minigun
**** DONE Impact enemy entities
**** DONE Deal damage
*** Flak cannon
**** DONE Shell
**** DONE Shell damages enemies
**** DONE Sprite work
*** Fire-and-forget missile
** Menus
** Levels
*** DONE Level 1: Hardware Functional
*** DONE Level 2: Escalation
*** DONE Level 3: Clearing the Skies
*** DONE Level 4: On All Sides
*** TODO Level 5: Chances of Surrender: Unlikely
** Infinite mode?

* Notes
** The good
*** Godot Engine, the good parts
**** Composing is easy
**** The particle system gets you quite far
**** Superb animation system, very simple to animate node properties
**** Engine provides most things for you
** The bad
*** GDScript
**** Types are not well thought out
**** Splitting up code functionality by file is not well supported, modularity is by component
*** Refactoring is annoyingly difficult
**** Extending previous nodes is possible, but it's easy to fall into a pattern of copy-pasting
**** Reorganizing scenes usually more or less means rebuilding the scene
*** Engine bugs
**** Starting timers inside a timer timeout is unstable
*** Process
**** A lot of time spent on drawing art, could have been avoided
