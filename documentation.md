# Incoming!

## READ THIS: In case of trouble

The Godot Engine editor **may crash** on first import of assets. This is a bug
on Godot's side and there is seemingly nothing I can do to fix this. The project
**should load fine after restarting the editor**. The bug has something to do
with creating thumbnails for custom fonts.

The issue has been fixed upstream and everything should work when Godot 3.3.1 is
released: https://github.com/godotengine/godot/pull/48302

## Architecture

Godot Engine uses a very simple hierarchical scene graph, and essentially all
parts of the game simply slot somewhere into that graph.

All of the mechanical parts of the game exist under the ```scenes``` folder,
which itself splits into the following sub-folders:

- ```effects```, containing scenes that represent visual effects (fire, explosions)
- ```enemies```, hostile enemy entities and entities related to them
- ```levels```, the playable levels, each inheriting from the Gameplay scene
- ```menus```, menus and UI elements
- ```player```, entities that constitute the player's ship, such as weapons, HUD etc.

The scenes generally speaking are split into either parts of scenes or full
scenes that the game will change between. The levels for example are implemented
as scenes that will be loaded and fully replace the current scene, whereas
single enemies and UI elements are usually just used as parts of a full scene.

Additionally some code is stored in the form of singleton scripts under the
```singletons``` folder. These are automatically loaded by the game engine, and
exist outside of the hierarchical structure of the rest of the scenes. They deal
with storing and providing access to various pieces of global data, like the
player's health, current level, number of enemies etc. The Level Manager
singleton is also responsible for loading and saving progress onto disk.

Finally the ```sfx```, ```fonts``` and ```sprites``` folders contain purely
static assets.

## Implementation details

Most entities in the game follow a pretty basic formula to their creation. The
game uses Godot's 2D physics system, which provides the necessary collision
detection system. 

By using different collision layers and masks for projectiles and target
entities, the projectiles can register a hit correctly and then use Godot's
signal and event systems to communicate with things they collide with. This
decouples entities from one another and no sharing of memory needs to take place
to change another entity's state. 

The project also takes advantage of Godot Engine's scene inheritance,
particularly in creating levels. Each of the levels inherits from the Gameplay
scene, which itself sets up the player entity, HUD elements and other basic
blocks of gameplay. The level scenes themselves simply designate where and when
enemies will spawn using Spawner entities, which designate a path along which
specific enemy types will be spawned and values for timing when those enemies
are supposed to spawn.

The combination of inherited scenes and spawners made level creation a breeze,
since level creation didn't require recreating the entire scene and placing down
individual enemies could be avoided by simply scripting the spawning mechanics.

The game also heavily takes advantage of Godot's particle system to pull off
different graphical effects. Explosions, fire, rocket trails and the randomly
spawning clouds all are implemented with particles. The particle system in Godot
is very flexible and with just a little bit of tuning can produce good results
without needing to tap into shader code.

## Testing

Because this type of game development doesn't work too well with automated
testing, and because Godot Engine's testing facilities are still
work-in-progress, the game has been only tested manually.

However, the web builds of the game were used to create a regularly updated
version of the game with a low barrier to entry, and this was utilized to bring
in external testers in the form of Internet friends. Feedback from the testers
was used to adjust and change certain features of the game, resulting in an
improved product.

## Time estimation

Approximately 25 hours of work total went into creating the game. This includes
time spent planning the mechanics, studying the Godot Engine documentation,
drawing the sprite work, finding proper sound effects, coding and testing the
different mechanics and levels. Additionally some time was spent bug hunting an
issue with importing the project, which yielded an actual upstream bug and a
subsequent upstream fix.
