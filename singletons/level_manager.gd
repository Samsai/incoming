extends Node


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var last_completed = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	load_game()

func save_game():
	var save_object = {
		"last_completed_level" : last_completed
	}
	
	var save_file = File.new()
	save_file.open("user://savegame.sav", File.WRITE)
	
	save_file.store_line(to_json(save_object))

func load_game():
	var save_file = File.new()
	if not save_file.file_exists("user://savegame.sav"):
		return
	
	save_file.open("user://savegame.sav", File.READ)
	
	var save_object = parse_json(save_file.get_line())
	
	last_completed = save_object["last_completed_level"]
