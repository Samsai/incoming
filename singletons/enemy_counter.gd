extends Node


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var mutex: Mutex = Mutex.new()
var total_enemies = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# To ensure our counter is thread safe, we use a mutex
func increment_counter(x):
	mutex.lock()
	total_enemies += x
	mutex.unlock()
	
	print("Number of enemies: " + str(total_enemies))

func decrement_counter():
	mutex.lock()
	total_enemies -= 1
	mutex.unlock()
	
	print("Number of enemies: " + str(total_enemies))

func reset():
	mutex.lock()
	total_enemies = 0
	mutex.unlock()
