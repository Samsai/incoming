extends Node


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

const MINIGUN = 0
const FLAKCANNON = 1

var selected_weapon = MINIGUN

var health = 100
var minigun_ammo = 2000
var flak_ammo = 50

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func reset():
	health = 100
	minigun_ammo = 2000
	flak_ammo = 50
