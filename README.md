# Incoming!

Incoming! is a game I developed for a Game Project Course in university. It's a simple 2D turret shooter developed with the Godot Engine in the span of about two months.

You can find downloads to the game over at https://samsai.itch.io/incoming

# About

You take command of the anti-aircraft weaponry of a destroyer, and your task is to protect your ship from incoming hostile missiles and shoot down the enemy aircraft as they pass.

The game consists of 5 levels of increasing difficulty, featuring 4 types of enemies: jets, helicopters, bombers and cruise missiles. You have an arsenal of two miniguns and a flak gun to take on the waves of hostile forces. Your miniguns are your main defense against incoming projectiles, although they are weak against aircraft and have a limited range. Your long range option for dealing with aircraft is the flak gun, which will file a projectile that will detonate in the air destroying nearby enemies, excluding missiles.

Victory is reached by destroying all enemies.

# Licensing

Refer to CREDITS for third-party licensing information.

All code is GPLv3, sprites Creative Commons Attribution 4.0, sound effects and music according to separate licensing as noted in the CREDITS files.
